import Vuex from 'vuex';

const modules = {

};

const state = {
};

const getters = {
};

const actions = {
    nuxtServerInit({ commit }) {
    },
    nuxtClientInit({ dispatch }) {
    },
};

const mutations = {
};

const createStore = () => new Vuex.Store({
    modules,
    state,
    getters,
    actions,
    mutations,
});

export default createStore;
